from django.db import models


class Menu(models.Model):
    name = models.CharField(max_length=200)
    parent = models.ForeignKey('self',on_delete=models.CASCADE,null=True,blank=True)
    file = models.FileField(upload_to='files/',null=True,blank=True)
    def __str__(self):
        return self.name
    def menus(self):
        return Menu.objects.filter(parent=self).all()
