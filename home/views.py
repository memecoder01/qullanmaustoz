from .models import Menu
from django.shortcuts import render

# Create your views here.
def home(request):
    
    return render(request, 'base.html',)

def about(request):
    
    return render(request, 'about.html',)

def gallery(request):
    
    return render(request, 'gallery.html',)

def faq(request):
    
    return render(request, 'faq.html',)

def contact(request):
    
    return render(request, 'contact.html',)

def menu(request):
    context = {
        'menu': Menu.objects.filter(parent=None)
    }
    return render(request, 'menu.html',context)