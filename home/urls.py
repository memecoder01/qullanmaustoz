from django.urls import path
from .views import *
urlpatterns = [
    path('', home, name='home'),
    path('menu/', menu, name='menu'),
    path('about', about, name='about'),
    path('gallery', gallery, name='gallery'),
    path('faq/', faq, name='faq'),
    path('contact/', contact, name='contact'),
]